## Das Primäre und das Sekundäre

<div class="handwritten">
Es ist so verdammt schwer nicht zu bewerten, nicht es so umzumachen, umzuwandeln, zu etwas, damit man damit fertig ist.
Die beiden sind sich so ähnlich das Lichte und das Unsichtbare, und trotzdem macht es da drinnen so einen großen Unterschied.
Gerade wenn du es sagst, dann ist es sonnenklar für mich, was da fließend im Bewusstsein ist und was konkret in der Wirklichkeit da ist.
Aber sobald ich mit anderen zusammen bin, die nicht zu denken, die nur sagen wie es in der Wirklichkeit ist, dann werde ich dadurch mitgenommen und ich werde dafür oder dagegen.
Und ich antworte dafür oder dagegen, anstatt zu denken wie du sagst.
Wie soll es mir möglich sein, in diesem Offenen in mir zu verbleiben, wenn andere das nicht sind.
</div>

Du weißt, ich rede über das Primäre und das Sekundäre.
Dass das Primäre das ist, es ist nur, ob wir daran denken oder nicht.
Darüber hinaus können wir das Bewusstsein über uns selber losmachen oder das Bewusstsein um andere losmachen, um die Welt und um die Wirklichkeit losmachen.
Wir können es als Phänomene untersuchen, oder wir können es bewerten.
Die Fähigkeit, nicht ausgeliefert zu sein, einer einzigen Wahrheit oder getrieben zu sein durch einen Trieb, diese Fähigkeit ist da.
Auch wenn wir starke Gesellschaftsprägungen in unserer Zivilisation haben.
Es ist das Tief-Menschliche.
Und darum brauchen wir behutsam sein und dieses pflegen, so dass wir nicht steckenbleiben in irgendeiner Art von Dogma, und dann dieses Dogma verteidigen müssen.
Und uns bedroht fühlen durch die Ansichten und die Dogmata anderer Menschen.
Wenn wir in dem sind, was wir von anfangen mitbringen, Vertrauen, Geborgenheit, Gemeinsamkeit und Neugierde, dann entwickeln wir uns in der hellen Seite in uns.
Wir sind kreativ, operativ und wir erforschen die Welt und die Wirklichkeit.
Am liebsten gemeinsam mit anderen.
Es ist nur, es geschieht einfach.
Damit sind wir programmiert.
