## Was tue ich, wenn gegen etwas argumentieren will?

<div class="handwritten">
Wie kann ich innehalten und nicht hängen bleiben an den Ansichten des anderen.
Ich reagiere sofort und ich muss einfach dagegen argumentieren.
Es ist wie ein Zwang, denn sonst wird der andere meinen, dass ich seiner Meinung bin.
Wenn ich gar nicht der Meinung bin, wie kann ich stattdessen tun?
</div>

Wenn der Impuls kommt zu antworten, den anderen mit dem Blick ein Moment lang verlässt, den Blick zu dir selber wendest und die Hand auf die Brust legst.
Unter der Hand wird es warm und du gehst in den Körper.
Dann wirst du anders antworten, anders als wenn du bleibst in der Bewertung oder in dem Dogma vom anderen.
Du wirst noch in Kontakt sein, aber außerhalb der Ladung vom andern.
Wenn du das üben kannst und es tun kannst bis es dir natürlich wird, dann bist du ein gutes Stück des Weges gekommen.
Es braucht einige Jahre, dann wirst du in deinem Innern frei sein, durch das Verantwortlichsein für das, was du selber ausdrückst.
Dann ist es uns möglich, wenn wir in dieses hineingereift sind, in das Sekundäre zu gehen, in das, was Elementar ist, was nicht existenziell unsterblich ist, und was die Wahlmöglichkeiten da sein lassen, und was das Wählbare sichtbar macht.

Dann können wir das eine oder das andere wählen, wir können sowohl als auch wählen oder keins von all dem.
Wenn wir keins von beiden wählen, dann fangen wir von vorne an und wählen einen anderen Weg.
Und wir können dies unzählige Male wiederholen.
So viele Male wie es uns einfällt.
Dann ist es ungefährlich, spannend und macht Spaß.
Wir bekommen Erfahrungen und wir entwickeln uns auf eine einzigartige Weise.

Es befreit uns davon, dass es auf eine gewisse Weise sein muss.
Wir entkommen Verhedderungen, und Überraschungen und Veränderungen in unserer Gedankenwelt werden möglich.
Und das bereichert uns innen.
